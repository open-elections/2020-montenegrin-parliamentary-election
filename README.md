# 2020 Montenegrin parliamentary election

    start_date: 2020-08-30
    end_date: 2020-08-30
    source: https://dik.co.me/ukupni-rezultati-izbora-za-poslanike-u-skupstinu-crne-gore-2/
    wikipedia: https://en.wikipedia.org/wiki/2020_Montenegrin_parliamentary_election

**The candidates file might not specify which candidates should be active. If you want any candidates to show up on Open Elections, add a column with the header `active` and add `TRUE` to any candidate you want to include.**
